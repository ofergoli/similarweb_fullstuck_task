const express = require('express');
const app = express();
const cors = require('cors');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const PORT = process.env.PORT || 3070;

//************************************************************************************************************//
//
// For scaling i would add monitoring tool such as NewRelic or DataDog
// And monitor the cpu utilization, memory usage, amount of request per second etc
// in order to be able let the ops team create more instances / add more containers / pods on kubernetes etc
//
//
//	const newrelic = require('newrelic');
//  app.locals.newrelic = newrelic;
//
//
//************************************************************************************************************//

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
app.use(cors());
app.use(morgan('combined'));

async function start() {
	try{
		global.dataDB = await require('./models').dataDBsqlConnect();
		await listen(app);
		await initiateRoutes();
		console.log(`Similar Web App up and running on port ${PORT}`);
	}
	catch (error) {
		console.error('Similar Web App down', error);
	}
}

const initiateRoutes = () => {
	const trendApi = require('./controller/trending-api');
	app.use('/trendingPage', trendApi);
};

const listen = app => new Promise((resolve, reject) => {
	app.listen(PORT, err => {
		if (err) {
			console.error('Similar Web App down');
			return reject(err);
		}
		return resolve();
	});
});

process.on('uncaughtException', error => {
	console.error('Similar Web: uncaughtException:', error);
	process.exit(1);
});

start();
