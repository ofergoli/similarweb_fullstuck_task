const mysql = require('mysql');
const {host, password, user, database, port} = require('./../config/mysql')
const {connectionLimit} = require('./../config/scale.config');

const query = (q, params) => new Promise((resolve, reject) => {
	global.dataDB.query(q, params, (err, data) => {
		if (err) {
			return reject(err);
		}
		return resolve(data);
	})
});

function dataDBsqlConnect() {
	const pool = mysql.createPool({
		connectionLimit,
		host,
		user,
		password,
		port,
		database
	});
	return pool;
}

module.exports = {
	dataDBsqlConnect,
	promisedQuery: query
};
