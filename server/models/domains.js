const FETCH_DOMAIN_QUERY = `select * from domains`;

const fetchDomain = async () => await promisedQuery(FETCH_DOMAIN_QUERY, []);

module.exports = {
	fetchDomain
};
