module.exports = {
	HTTP_STATUS_CODES: {
		SUCCESS: {
			OK: 200,
		},
		FAILURE: {
			NOT_FOUND: 404,
		}
	}
}
