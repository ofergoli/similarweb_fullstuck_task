const DB_USER = process.env.DB_USER || `user`;
const DB_PASSWORD = process.env.DB_PASSWORD || `passpass`;
const DB_DATABASE = process.env.DB_DATABASE || `trending`;
const DB_HOST = process.env.DB_HOST || `interview-trending.cknxcwudwsyr.us-east-1.rds.amazonaws.com`;

module.exports = {
	name: `similarweb`,
	user: DB_USER,
	password: DB_PASSWORD,
	connector: 'mysql',
	database: DB_DATABASE,
	port: 3306,
	host: DB_HOST,
}
