const express = require('express');
const _ = require('lodash');
const router = express.Router();
const {fetchDomain} = require('../services/trending-service');
const {HTTP_STATUS_CODES: {FAILURE: {NOT_FOUND}, SUCCESS: {OK}}} = require('../config/app-config');

const errHandler = (res) => res.sendStatus(NOT_FOUND);
const resolveResponse = (res, data) => res.status(OK).json(data);

router.get('/:domain', async (req, res) => {
	const {domain} = req.params;
	try {
		const result = _.head(await fetchDomain(domain));
		if (!result) {
			throw new Error('Cant find any records');
		}
		return resolveResponse(res, result);
	} catch(err) {
		return errHandler(res);
	}
});

module.exports = router;
