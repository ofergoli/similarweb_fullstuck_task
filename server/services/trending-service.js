const FETCH_DOMAIN_QUERY = `select * from trending_per_domain where domain_name=?`;

const {promisedQuery} = require('./../models/index');

module.exports = {
	fetchDomain: async domain => await promisedQuery(FETCH_DOMAIN_QUERY, [domain]),
}
