# Similar Web Fullstack Task Api

Go to /server and install 
```
npm install
```

Run
```
node app.js
```


The server is running on port 3070.

```
http://localhost:3070
```

Add the extention to chrome. If the domain doesn't matches any record should show no result page otherwise will show 3 top trends.

License
----

Ofer Golibroda
