const fetchDomain = domain => axios.get(`http://localhost:3070/trendingPage/${domain}`);

const hideSpinner = () => {
	document.getElementById('spinner').classList.remove('show');
	document.getElementById('spinner').classList.add('hide');
}

const buildPage = ({data}) => {
	const domain = data['domain_name'];
	document.getElementById('domain_spot').innerText = domain;
	const list = document.getElementById('collections_list');
	Object.values(data).filter(x => x !== domain).forEach(trend => {
		const entry = document.createElement('div');
		entry.classList.add('collection-item');
		entry.innerText = trend;
		list.appendChild(entry);
	});
	document.getElementById('content_box').classList.remove('hide');
	document.getElementById('content_box').classList.add('show');
	return hideSpinner();
}

const renderNoResults = (domain) => {
	document.getElementById('no_results').classList.remove('hide');
	document.getElementById('no_results').classList.add('show');
	document.getElementById('no_results_domain').innerText = domain;
	return hideSpinner();
}

chrome.tabs.query({'active': true, 'windowId': chrome.windows.WINDOW_ID_CURRENT}, tabs => {
	if (tabs[0] && tabs[0].url) {
		const domain = new URL(tabs[0].url).host.replace('www.', '');
		return fetchDomain(domain).then(buildPage).catch(() => renderNoResults(domain));
	}
});
